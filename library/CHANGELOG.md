# Changelog

## `0.5.0` 2022-03-29

- Make relative URLs absolute, where possible

## `0.4.0` 2022-03-24

- Remove `photo`. Instead use `featured`

## `0.3.0` 2022-03-24

- Add `audio` if present from Twitter metadata

## `0.2.1` 2022-03-24

- Handle multi-value or single-value photo properties

## `0.2.0` 2022-03-14

- Add `featured` property for sharing image metadata

## `0.1.0` 2022-03-14

- Initial release, with key functionality available
