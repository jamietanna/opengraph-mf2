const url = require('url')

function setName (result, properties) {
  if (result.ogTitle) {
    properties.name = [result.ogTitle]
  } else if (result.twitterTitle) {
    properties.name = [result.twitterTitle]
  }
}

function setSummary (result, properties) {
  if (result.ogDescription) {
    properties.summary = [result.ogDescription]
  } else if (result.twitterDescription) {
    properties.summary = [result.twitterDescription]
  }
}

function setPhoto (result, properties) {
  let images = null
  if (result.ogImage) {
    images = parseImages(result.ogImage)
  } else if (result.twitterImage) {
    images = parseImages(result.twitterImage)
  }
  if (images !== null) {
    properties.featured = images
  }
}

function setAudio (result, properties) {
  if (result.ogAudioSecureURL) {
    properties.audio = [result.ogAudioSecureURL]
  } else if (result.ogAudioURL) {
    properties.audio = [result.ogAudioURL]
  } else if (result.twitterPlayer && result.twitterPlayer.stream) {
    properties.audio = [result.twitterPlayer.stream]
  }
}

function setAuthor (result, properties) {
  if (result.author) {
    properties.author = [{
      type: ['h-card'],
      properties: {
        name: [result.author]
      }
    }]
  }
}

function handleRelativeUrl (result, theUrl) {
  const parsed = new url.URL(theUrl)
  if (parsed.host !== null) {
    return theUrl
  }

  return result.requestUrl
}

function setUrl (result, properties) {
  let theUrl = null
  if (result.ogUrl) {
    theUrl = handleRelativeUrl(result, result.ogUrl)
  } else if (result.requestUrl) {
    theUrl = handleRelativeUrl(result, result.requestUrl)
  }
  if (theUrl) {
    properties.url = [theUrl]
  }
}

function setPublished (result, properties) {
  if (result.articlePublishedTime) {
    properties.published = [result.articlePublishedTime]
  }
}

function setUpdated (result, properties) {
  if (result.articleModifiedTime) {
    properties.published = [result.articleModifiedTime]
  }
}

function toMf2 (result) {
  const properties = {}
  setName(result, properties)
  setSummary(result, properties)
  setPhoto(result, properties)
  setAudio(result, properties)
  setAuthor(result, properties)
  setUrl(result, properties)
  setPublished(result, properties)
  setUpdated(result, properties)
  return {
    type: ['h-entry'],
    properties
  }
}

function parseImages (property) {
  let images = null
  if (property) {
    if (Array.isArray(property) && property.length !== 0) {
      images = property.map(function (element) {
        return element.url
      })
    } else {
      images = [property.url]
    }
  }
  return images
}

module.exports = toMf2
