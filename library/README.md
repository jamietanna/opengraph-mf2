# OpenGraph to Microformats2 converter

A library to convert between parsed [OpenGraph metadata](https://ogp.me/) to a [Microformats2 object](https://microformats.io).

## Installation

To install it for your current project, you can run:

```sh
npm install --save @jamietanna/opengraph-mf2
```

## Usage

```js
const toMf2 = require('@jamietanna/opengraph-mf2');

// for a given, parsed set of OpenGraph metadata, for instance using https://www.npmjs.com/package/open-graph-scraper+
const og = {
  "ogTitle": "Week Notes 22#10 · Jamie Tanna | Software Engineer",
  "ogLocale": "en_GB",
  "ogDescription": "What happened in the week of 2022-03-07?",
  "ogUrl": "https://www.jvt.me/week-notes/2022/10/",
  "ogSiteName": "Jamie Tanna | Software Engineer",
  "ogType": "article",
  "articlePublishedTime": "2022-03-13T21:59:14+0000",
  "twitterSite": "@jamietanna",
  "twitterCreator": "@jamietanna",
  "twitterCard": "summary",
  "twitterTitle": "Week Notes 22#10 · Jamie Tanna | Software Engineer",
  "twitterDescription": "What happened in the week of 2022-03-07?",
  "ogImage": {
    "url": "https://www.jvt.me/img/profile.jpg",
    "width": null,
    "height": null,
    "type": "jpg"
  },
  "twitterImage": {
    "url": "https://www.jvt.me/img/profile.jpg",
    "width": null,
    "height": null,
    "alt": null
  },
  "ogDate": "2022-03-13T21:59:14+0000",
  "favicon": "/favicon.png",
  "charset": "utf8",
  "requestUrl": "https://www.jvt.me/week-notes/2022/10/",
  "success": true
};

const mf2 = toMf2(og);
/*
mf2 = {
  type: [ 'h-entry' ],
  properties: {
    name: [ 'Week Notes 22#10 · Jamie Tanna | Software Engineer' ],
    summary: [ 'What happened in the week of 2022-03-07?' ],
    photo: [ 'https://www.jvt.me/img/profile.jpg' ],
    url: [ 'https://www.jvt.me/week-notes/2022/10/' ],
    published: [ '2022-03-13T21:59:14+0000' ]
  }
}
*/
```
