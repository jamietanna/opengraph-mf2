# OpenGraph to Microformats2

A set of libraries and tools to convert between parsed [OpenGraph metadata](https://ogp.me/) to a [Microformats2 object](https://microformats.io).
