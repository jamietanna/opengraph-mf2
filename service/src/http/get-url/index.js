const logger = require('@architect/shared/logger')
const toMf2 = require('@jamietanna/opengraph-mf2')
const htmlResponse = require('./html')
const ogs = require('open-graph-scraper')

exports.handler = async function http (req) {
  const url = req.queryStringParameters?.url

  if (url === undefined) {
    return await htmlResponse.render(url, null)
  }

  const { error, result } = await ogs({ url }).catch(error => error)
  const container = { items: [] }

  if (error) {
    logger.error({
      message: `Failed to retrieve OpenGraph data for ${url}`, userAgent: `${JSON.stringify(req.headers['user-agent'])}`, result: error.result
    })
    return {
      statusCode: 400,
      headers: {
        'cache-control': 'no-cache, no-store, must-revalidate, max-age=0, s-maxage=0',
        'content-type': 'application/mf2+json; charset=utf8'
      },
      body: JSON.stringify(container)
    }
  }

  const mf2 = toMf2(result)
  container.items.push(mf2)

  logger.info({ message: `Generating Microformats2 representation for ${url}`, userAgent: `${JSON.stringify(req.headers['user-agent'])}`, mf2, result })

  if (await htmlResponse.isRequired(req)) {
    return await htmlResponse.render(url, mf2)
  }

  return {
    statusCode: 200,
    headers: {
      'cache-control': 'no-cache, no-store, must-revalidate, max-age=0, s-maxage=0',
      'content-type': 'application/mf2+json; charset=utf8'
    },
    body: JSON.stringify(container)
  }
}
