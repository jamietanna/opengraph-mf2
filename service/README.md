# OpenGraph Microformats2 Pipe

An [IndieWeb Pipe](https://indieweb.org/pipes) that produces an HTTP interface for the [opengraph-mf2 library](https://gitlab.com/jamietanna/opengraph-mf2/-/tree/main/library).

A version of this is hosted at [opengraph-mf2.tanna.dev](https://opengraph-mf2.tanna.dev) and is available for general consumption.
